﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Client_WFA
{
    class Utils
    {
        // хешь одно направленная функция
        public static string GetSHA256Hash(string textData)
        {
            using (SHA256 hashFunc = SHA256.Create())
            {
                byte[] bytes = hashFunc.ComputeHash(Encoding.UTF8.GetBytes(textData));
                // каждый байт надо превратить в 16 речное число

                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }

                return builder.ToString();


            }
        }
    }
}
