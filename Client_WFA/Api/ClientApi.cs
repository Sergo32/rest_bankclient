﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransferDataClassLibrary.Entities;
using Newtonsoft.Json;
using System.Net;
using System.Xml;
using Newtonsoft.Json.Linq;


namespace Client_WFA.Api
{
    class ClientApi
    {
        private XmlDocument document;
        private WebClient client = null;

        private string responseXml = "";
        private string responseJson = "";

        private static ClientApi instance = null;

        public static ClientApi GetInstance()
        {
            if (instance == null)
            {
                instance = new ClientApi();
            }
            return instance;
        }

        public User GetUserByLoginAndPassword(string login, string password)//высокоуровневая обертка
        {

            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                string d = client.DownloadString($"http://localhost:51623/ContractService.svc/rest/usersAuth?login={login}&password={password}");

                JObject jObject = JObject.Parse(d);


                User user = new User()
                {
                    Id = int.Parse(jObject["Id"].ToString()),
                    Fio = jObject["Fio"].ToString(),
                    Balance = int.Parse(jObject["Balance"].ToString())
                };
                return user;
            }
            catch
            {
                User user = null;
                return user;

            }


        }

        public bool RegisterUser(User user)//высокоуровневая обертка
        {

            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string uri = "http://localhost:51623/ContractService.svc/rest/userCreateNew";

            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            string json = JsonConvert.SerializeObject(user);
            string responseJson = client.UploadString(uri, "POST", json);

            bool result = JsonConvert.DeserializeObject<bool>(responseJson);
            return result;

        }



        public List<User> GetAllUsersWithoutMe(string explitId)
        {

            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                responseJson = client.DownloadString($"http://localhost:51623/ContractService.svc/rest/getAllUser/{explitId}");
                List<User> user = JsonConvert.DeserializeObject<List<User>>(responseJson);

                return user;
            }
            catch
            {
                List<User> user = null;
                return user;
            }

        }
        public int GetMyBalance(string id)
        {

            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            try
            {
                responseJson = client.DownloadString($"http://localhost:51623/ContractService.svc/rest/myBalance/{id}");
                int balance = JsonConvert.DeserializeObject<int>(responseJson);

                return balance;
            }
            catch
            {
                return 0;
            }

        }

        public bool UpMyBalance(Transaction transaction)
        {
            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string uri = "http://localhost:51623/ContractService.svc/rest/mybalanceUp";

            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            string json = JsonConvert.SerializeObject(transaction);
            string responseJson = client.UploadString(uri, "POST", json);

            bool result = JsonConvert.DeserializeObject<bool>(responseJson);
            return result;

        }


        public bool MakeTrasactionFromUserToUser(Transaction transaction)
        {
            client = new WebClient();
            client.Encoding = Encoding.UTF8;
            string url = $"http://localhost:51623/ContractService.svc/rest/userToUserTransaction";
            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            string json = JsonConvert.SerializeObject(transaction);
            string stringJson = client.UploadString(url, "POST", json);

            bool result = JsonConvert.DeserializeObject<bool>(stringJson);

            return result;
        }

        public List<Transaction> GetMyTransactionHistoryWithAnotherUser(int myId, int userId)
        {
            client = new WebClient();
            client.Encoding = Encoding.UTF8;

            Transaction transactionHistory = new Transaction()
            {
                UserFrom = new User() { Id = myId },
                UserTo = new User() { Id = userId }
            };
            try
            {
                string url = $"http://localhost:51623/ContractService.svc/rest/getMyTransactionHistoryWithAnotherUser";
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                string json = JsonConvert.SerializeObject(transactionHistory);
                responseJson = client.UploadString(url, "POST", json);
                List<Transaction> historyTransactions = JsonConvert.DeserializeObject<List<Transaction>>(responseJson);

                return historyTransactions;
            }
            catch
            {
                return null;
            }
        }



    }
}
