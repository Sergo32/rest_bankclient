﻿namespace Client_WFA
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelSelectedUserToId = new System.Windows.Forms.Label();
            this.textBoxSelectedUserId = new System.Windows.Forms.TextBox();
            this.buttotGetTranferHistori = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.richTextBoxTransferHistori = new System.Windows.Forms.RichTextBox();
            this.buttonTransfer = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.maskedTextBoxTransferMoney = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.maskedTextBoxIncreaseBalance = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonIncreaseBalance = new System.Windows.Forms.Button();
            this.labelFio = new System.Windows.Forms.Label();
            this.labelBalans = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // labelSelectedUserToId
            // 
            this.labelSelectedUserToId.AutoSize = true;
            this.labelSelectedUserToId.Location = new System.Drawing.Point(579, 42);
            this.labelSelectedUserToId.Name = "labelSelectedUserToId";
            this.labelSelectedUserToId.Size = new System.Drawing.Size(101, 17);
            this.labelSelectedUserToId.TabIndex = 41;
            this.labelSelectedUserToId.Text = "Пользователь";
            // 
            // textBoxSelectedUserId
            // 
            this.textBoxSelectedUserId.Location = new System.Drawing.Point(771, 44);
            this.textBoxSelectedUserId.Name = "textBoxSelectedUserId";
            this.textBoxSelectedUserId.Size = new System.Drawing.Size(100, 22);
            this.textBoxSelectedUserId.TabIndex = 40;
            this.textBoxSelectedUserId.Visible = false;
            // 
            // buttotGetTranferHistori
            // 
            this.buttotGetTranferHistori.Location = new System.Drawing.Point(630, 355);
            this.buttotGetTranferHistori.Name = "buttotGetTranferHistori";
            this.buttotGetTranferHistori.Size = new System.Drawing.Size(190, 23);
            this.buttotGetTranferHistori.TabIndex = 39;
            this.buttotGetTranferHistori.Text = "Получить историю";
            this.buttotGetTranferHistori.UseVisualStyleBackColor = true;
            this.buttotGetTranferHistori.Click += new System.EventHandler(this.buttotGetTranferHistori_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(567, 141);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 17);
            this.label9.TabIndex = 38;
            this.label9.Text = "История переводов";
            // 
            // richTextBoxTransferHistori
            // 
            this.richTextBoxTransferHistori.Location = new System.Drawing.Point(570, 161);
            this.richTextBoxTransferHistori.Name = "richTextBoxTransferHistori";
            this.richTextBoxTransferHistori.ReadOnly = true;
            this.richTextBoxTransferHistori.Size = new System.Drawing.Size(298, 175);
            this.richTextBoxTransferHistori.TabIndex = 37;
            this.richTextBoxTransferHistori.Text = "";
            // 
            // buttonTransfer
            // 
            this.buttonTransfer.Location = new System.Drawing.Point(771, 82);
            this.buttonTransfer.Name = "buttonTransfer";
            this.buttonTransfer.Size = new System.Drawing.Size(97, 23);
            this.buttonTransfer.TabIndex = 36;
            this.buttonTransfer.Text = "Перевести";
            this.buttonTransfer.UseVisualStyleBackColor = true;
            this.buttonTransfer.Click += new System.EventHandler(this.buttonTransfer_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(576, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 35;
            this.label8.Text = "Перевести";
            // 
            // maskedTextBoxTransferMoney
            // 
            this.maskedTextBoxTransferMoney.Location = new System.Drawing.Point(579, 82);
            this.maskedTextBoxTransferMoney.Mask = "000000";
            this.maskedTextBoxTransferMoney.Name = "maskedTextBoxTransferMoney";
            this.maskedTextBoxTransferMoney.Size = new System.Drawing.Size(186, 22);
            this.maskedTextBoxTransferMoney.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(576, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 17);
            this.label5.TabIndex = 33;
            this.label5.Text = "Выбранный пользователь";
            // 
            // maskedTextBoxIncreaseBalance
            // 
            this.maskedTextBoxIncreaseBalance.Location = new System.Drawing.Point(15, 391);
            this.maskedTextBoxIncreaseBalance.Mask = "000000";
            this.maskedTextBoxIncreaseBalance.Name = "maskedTextBoxIncreaseBalance";
            this.maskedTextBoxIncreaseBalance.Size = new System.Drawing.Size(167, 22);
            this.maskedTextBoxIncreaseBalance.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 358);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Пополнить баланс на";
            // 
            // buttonIncreaseBalance
            // 
            this.buttonIncreaseBalance.Location = new System.Drawing.Point(12, 419);
            this.buttonIncreaseBalance.Name = "buttonIncreaseBalance";
            this.buttonIncreaseBalance.Size = new System.Drawing.Size(166, 23);
            this.buttonIncreaseBalance.TabIndex = 30;
            this.buttonIncreaseBalance.Text = "Пополнить";
            this.buttonIncreaseBalance.UseVisualStyleBackColor = true;
            this.buttonIncreaseBalance.Click += new System.EventHandler(this.buttonIncreaseBalance_Click);
            // 
            // labelFio
            // 
            this.labelFio.AutoSize = true;
            this.labelFio.Location = new System.Drawing.Point(15, 44);
            this.labelFio.Name = "labelFio";
            this.labelFio.Size = new System.Drawing.Size(42, 17);
            this.labelFio.TabIndex = 29;
            this.labelFio.Text = "ФИО";
            // 
            // labelBalans
            // 
            this.labelBalans.AutoSize = true;
            this.labelBalans.Location = new System.Drawing.Point(12, 102);
            this.labelBalans.Name = "labelBalans";
            this.labelBalans.Size = new System.Drawing.Size(56, 17);
            this.labelBalans.TabIndex = 28;
            this.labelBalans.Text = "Баланс";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 27;
            this.label6.Text = "Ваш Баланс";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 200);
            this.label4.TabIndex = 26;
            this.label4.Text = "Выберите из списка пользователя \r\nкоторому хотите превести деньги\r\n\r\nВведите сумм" +
    "у и\r\nнажмите \"отправить\"\r\nИмя\r\nВведите нужную сумму \r\nнажмите \"пополнить\"\r\n";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Здравствуйте";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Список доступных пользователей";
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.ItemHeight = 16;
            this.listBoxUsers.Location = new System.Drawing.Point(264, 44);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(275, 356);
            this.listBoxUsers.TabIndex = 23;
            this.listBoxUsers.SelectedIndexChanged += new System.EventHandler(this.listBoxUsers_SelectedIndexChanged);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 4000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 481);
            this.Controls.Add(this.labelSelectedUserToId);
            this.Controls.Add(this.textBoxSelectedUserId);
            this.Controls.Add(this.buttotGetTranferHistori);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.richTextBoxTransferHistori);
            this.Controls.Add(this.buttonTransfer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.maskedTextBoxTransferMoney);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.maskedTextBoxIncreaseBalance);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonIncreaseBalance);
            this.Controls.Add(this.labelFio);
            this.Controls.Add(this.labelBalans);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxUsers);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSelectedUserToId;
        private System.Windows.Forms.TextBox textBoxSelectedUserId;
        private System.Windows.Forms.Button buttotGetTranferHistori;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox richTextBoxTransferHistori;
        private System.Windows.Forms.Button buttonTransfer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxTransferMoney;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxIncreaseBalance;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonIncreaseBalance;
        private System.Windows.Forms.Label labelFio;
        private System.Windows.Forms.Label labelBalans;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBoxUsers;
        private System.Windows.Forms.Timer timerUpdate;
    }
}