﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client_WFA.Api;
using TransferDataClassLibrary.Entities;

namespace Client_WFA
{
    public partial class FormRegister : Form
    {
       // private XmlDocument document;
        //private WebClient client = null;
        private ClientApi api;
        public FormRegister()
        {
            InitializeComponent();
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            

            if (textBoxFio.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. заполните fio");
                return;
            }

            if (textBoxLogin.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Заполните логин");
                return;
            }
            if (textBoxPassword.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Заполните пароль");
                return;
            }

            try
            {

                User user = new User()
                {
                    Fio = textBoxFio.Text,
                    Login = textBoxLogin.Text,
                    Password = Utils.GetSHA256Hash(textBoxPassword.Text)
                    

                };

                bool reqisterResult = api.RegisterUser(user);

                if (reqisterResult == true)
                {
                    MessageBox.Show("Ошибка Пользователь с таким логином существ");
                    return;
                }

                MessageBox.Show("Вы успешно зарегистрированы. Введите ваши данные на форме авторизации");
                this.Close();

            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка соединение с сервером. Попроюуйте через некоторое время. Текст ошибки " + exception);

            }
        }

        private void FormRegister_Load(object sender, EventArgs e)
        {
            api = ClientApi.GetInstance();// подключить        
        }
    }
}
