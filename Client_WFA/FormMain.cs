﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client_WFA.Api;
using TransferDataClassLibrary.Entities;

namespace Client_WFA
{
    public partial class FormMain : Form
    {
        private ClientApi api;

        public FormMain()
        {
            InitializeComponent();
        }


        private void FillRichTextBoxHistory(List<Transaction> transactions, int user1Id, int User2Id, string user1Fio, string user2Fio)
        {
            richTextBoxTransferHistori.Clear();

            foreach (Transaction transaction in transactions)
            {
                string fioFrom, fioTo;

                if (transaction.UserFrom.Id == user1Id)
                {
                    fioFrom = user1Fio;
                    fioTo = user2Fio;
                }
                else
                {
                    fioFrom = user2Fio;
                    fioTo = user1Fio;
                }

                richTextBoxTransferHistori.Text += "Дата перевода" + transaction.Dt + "\n";
                richTextBoxTransferHistori.Text += "Отправитель:" + fioFrom + "\n";
                richTextBoxTransferHistori.Text += "Получатель:" + fioTo + "\n";
                richTextBoxTransferHistori.Text += "Сумма перевода:" + transaction.Money + "\n";
                richTextBoxTransferHistori.Text += "             \n";

            }
        }

        private void buttonIncreaseBalance_Click(object sender, EventArgs e)
        {
            if (maskedTextBoxIncreaseBalance.Text == "")
            {
                MessageBox.Show("Ошибка. Введите сумму пополнения");
            }

            try
            {
                int money = int.Parse(maskedTextBoxIncreaseBalance.Text);
                Transaction transaction = new Transaction()
                {
                    Money = money,
                    UserFrom = new User { Id = GlobalData.User.Id },
                    UserTo = new User() { Id = GlobalData.User.Id }
                };

                bool inreaseResult =  api.UpMyBalance(transaction);
                if (inreaseResult == true)
                {
                    int balance =  api.GetMyBalance(GlobalData.User.Id.ToString());
                    labelBalans.Text = balance.ToString();

                    maskedTextBoxIncreaseBalance.Clear();

                    MessageBox.Show("Баланс пополнен");
                }
                else
                {
                    MessageBox.Show("Ошибка пополнения баланса");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка соединение с сервером. Попроюуйте через некоторое время. Текст ошибки " + exception);

            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            api = ClientApi.GetInstance();

            try
            {

                labelFio.Text = GlobalData.User.Fio;
                labelBalans.Text = GlobalData.User.Balance.ToString();

                listBoxUsers.DataSource = api.GetAllUsersWithoutMe(GlobalData.User.Id.ToString());
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка соединение с сервером. Попроюуйте через некоторое время. Текст ошибки " + exception);

            }
        }

        private void buttonTransfer_Click(object sender, EventArgs e)
        {
            if (textBoxSelectedUserId.Text == "")
            {
                MessageBox.Show("Ошибка. Ввеберите пользователя для перевода");
                return;
            }


            if (maskedTextBoxTransferMoney.Text == "")
            {
                MessageBox.Show("Ошибка. Введите сумму пополнения");
                return;
            }

            try
            {
                int money = int.Parse(maskedTextBoxTransferMoney.Text);

                if (money >  api.GetMyBalance(GlobalData.User.Id.ToString()))
                {
                    MessageBox.Show("Увас недостаточно денег для перевода");
                    return;
                }

                int idUserTo = int.Parse(textBoxSelectedUserId.Text);
                Transaction transaction = new Transaction()
                {
                    Money = money,
                    UserFrom = new User { Id = GlobalData.User.Id },
                    UserTo = new User() { Id = idUserTo }
                };

                bool transactionResult = api.MakeTrasactionFromUserToUser(transaction);
                if (transactionResult == true)
                {
                    int balance =  api.GetMyBalance(GlobalData.User.Id.ToString());
                    labelBalans.Text = balance.ToString();


                    List<Transaction> transactions =  api.GetMyTransactionHistoryWithAnotherUser(GlobalData.User.Id, idUserTo);

                    FillRichTextBoxHistory(transactions, GlobalData.User.Id, idUserTo, labelFio.Text, labelSelectedUserToId.Text);


                    maskedTextBoxTransferMoney.Clear();
                    textBoxSelectedUserId.Clear();

                    MessageBox.Show("Перевод выполнен");
                }
                else
                {
                    MessageBox.Show("Ошибка Перевод баланса");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка соединение с сервером. Попроюуйте через некоторое время. Текст ошибки " + exception);

            }
        }

        private void buttotGetTranferHistori_Click(object sender, EventArgs e)
        {

            if (textBoxSelectedUserId.Text == "")
            {
                MessageBox.Show("Ошибка. Ввеберите пользователя для истории");
                return;
            }

            int idUserTo = int.Parse(textBoxSelectedUserId.Text);

            List<Transaction> transactions =  api.GetMyTransactionHistoryWithAnotherUser(GlobalData.User.Id, idUserTo);

            FillRichTextBoxHistory(transactions, GlobalData.User.Id, idUserTo, labelFio.Text, labelSelectedUserToId.Text);
        }

        private void listBoxUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            User user = listBoxUsers.SelectedItem as User;
            if (user == null)
            {
                return;
            }



            textBoxSelectedUserId.Text = user.Id.ToString();
            labelSelectedUserToId.Text = user.Fio;
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            int balance = api.GetMyBalance(GlobalData.User.Id.ToString());
            labelBalans.Text = balance.ToString();
        }
    }
}
