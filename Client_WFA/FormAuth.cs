﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client_WFA.Api;
using TransferDataClassLibrary.Entities;
using System.Net;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace Client_WFA
{
    public partial class FormAuth : Form
    {
       
        private ClientApi api;
        public FormAuth()
        {
            InitializeComponent();
        }

        private void buttonSignIn_Click(object sender, EventArgs e)
        {
           
            if (textBoxLogin.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Заполните логин");
                return;
            }
            if (textBoxPassword.Text == string.Empty)
            {
                MessageBox.Show("Ошибка. Заполните пароль");
                return;
            }

            try
            {

                User user = api.GetUserByLoginAndPassword(textBoxLogin.Text, Utils.GetSHA256Hash(textBoxPassword.Text));

                if (user == null)
                {
                    MessageBox.Show("Ошибка Логин или пароль");
                    return;
                }

                GlobalData.User = user;
                this.Hide();
                new FormMain().ShowDialog();
                Application.Exit();//в этом месте завершится программа 
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка соединение с сервером. Попроюуйте через некоторое время. Текст ошибки " + exception);

            }
        }

        private void FormAuth_Load(object sender, EventArgs e)
        {
            try
            {
                api = ClientApi.GetInstance();// подключить 
            }
            catch (Exception)
            {

                MessageBox.Show("Ошибка соединение с сервером. Приложение будет закрыто");
                Application.Exit();
            }
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            new FormRegister().ShowDialog();
        }

    
    }
}
