﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TransferDataClassLibrary.Entities;
using MySql.Data.MySqlClient;

namespace WcfService.Data
{
    public class TableTransaction
    {
        private string connectionString;

        public TableTransaction(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Insert(Transaction transaction)
        {

            try
            {

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"INSERT INTO `transaction`(`dt`,`userId`,`userIdTo`,`money`) VALUES('{DateTime.Now:yyyy-MM-dd HH:mm:ss}',{transaction.UserFrom.Id},{transaction.UserTo.Id},{transaction.Money})";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }


            }
            catch (Exception e)
            {
                throw e;

            }
        }

        public List<Transaction> GetAllTransactionBetweenUsers(int userId1, int userId2)
        {
            try
            {
                List<Transaction> usersTransaction = new List<Transaction>();

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"SELECT * From `transaction` WHERE `userId`={userId1} and `userIdTo`={userId2} OR `userId`={userId2} and `userIdTo`={userId1} ";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        while (reader.Read())
                        {
                            usersTransaction.Add(new Transaction()
                            {
                                Dt = reader.GetDateTime("dt").ToString(),
                                UserFrom = new User() { Id = reader.GetInt32("userId") },
                                UserTo = new User() { Id = reader.GetInt32("userIdTo") },
                                Money = reader.GetInt32("money")
                            });
                        }
                    }

                    mySqlConnection.Close();
                }

                return usersTransaction;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}