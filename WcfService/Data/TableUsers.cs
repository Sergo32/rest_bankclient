﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TransferDataClassLibrary.Entities;
using MySql.Data.MySqlClient;

namespace WcfService.Data
{
    public class TableUsers
    {
        private string connectionString;

        public TableUsers(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Insert(User user)
        {

            try
            {

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"INSERT INTO `users`(`fio`,`login`,`password`,`balance`) VALUES ('{user.Fio}','{user.Login}','{user.Password}',0)";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }


            }
            catch (Exception e)
            {
                throw e;

            }
        }



        public User SelectByLoginAndPassword(string login, string password)
        {
            try
            {
                User user = null;
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"SELECT `id`,`fio`,`balance`  From `users` WHERE `login`='{login}' AND `password`='{password}'";
                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        if (reader.Read())
                        {
                            user = new User()
                            {
                                Id = reader.GetInt32("id"),
                                Fio = reader.GetString("fio"),
                                Balance = reader.GetInt32("balance")

                            };
                        }
                    }

                    mySqlConnection.Close();
                }

                return user;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public bool ChekLogin(string login)
        {
            try
            {
                bool exist = false;//запретить добовлять пользователя если такой логин сушествует 

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"SELECT `id` From `users` WHERE `login`='{login}'";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        exist = reader.HasRows;
                    }

                    mySqlConnection.Close();
                }

                return exist;
            }
            catch (Exception e)
            {

                throw e;
            }


        }

        public List<User> GetAllUsersWithoutMe(int exsplitId)
        {
            try
            {
                List<User> users = new List<User>();

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"SELECT `id`,`fio` From `users` WHERE `id`!={exsplitId}";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        while (reader.Read())
                        {
                            users.Add(new User()
                            {
                                Id = reader.GetInt32("id"),
                                Fio = reader.GetString("fio")
                            });
                        }
                    }

                    mySqlConnection.Close();
                }

                return users;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public int GetMyBalance(int id)
        {
            try
            {
                int balance = 0;

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"SELECT `balance` From `users` WHERE `id`={id}";

                        balance = Convert.ToInt32(mySqlCommand.ExecuteScalar());// берет 1 строчку
                    }

                    mySqlConnection.Close();
                }

                return balance;

            }
            catch (Exception e)
            {

                throw e;
            }
        }


        public void IncreaseBalance(int id, int money)
        {

            try
            {

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Update  `users` SET  `balance`=`balance`+{money} WHERE `id`={id}";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }


            }
            catch (Exception e)
            {
                throw e;

            }
        }



        public void DecreaseBalance(int id, int money)
        {

            try
            {

                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Update  `users` SET `balance`=`balance`-{money} WHERE `id`={id}";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }


            }
            catch (Exception e)
            {
                throw e;

            }
        }
    }
}