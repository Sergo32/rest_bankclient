﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Data
{
    public class DbManager
    {
        private static DbManager instance = null;

        public static DbManager GetInstance()
        {
            if (instance == null)
            {
                instance = new DbManager();
            }

            return instance;
        }

        public TableUsers TableUsers { get; private set; }
        public TableTransaction TableTransaction { get; private set; }

        public DbManager()
        {
            string connectionString = "Server=localhost;User=root;Password=1234;Database=bank_system";

            TableUsers = new TableUsers(connectionString);
            TableTransaction = new TableTransaction(connectionString);

        }
    }
}