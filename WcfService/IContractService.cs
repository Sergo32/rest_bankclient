﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TransferDataClassLibrary.Entities;


namespace WcfService
{

    [ServiceContract]
    public interface IContractService
    {
        [OperationContract]
        [WebGet(UriTemplate = "usersAuth?login={login}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        User UsersAuth(string login, string password);

        [OperationContract]
        [WebGet(UriTemplate = "getAllUser/{id}", ResponseFormat = WebMessageFormat.Json)]
        List<User> GetAllUser(string id);

        [OperationContract]
        [WebGet(UriTemplate = "myBalance/{id}", ResponseFormat = WebMessageFormat.Json)]
        int MyBalance(string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/userToUserTransaction", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]//bare без обертки
        bool UserToUserTransaction(Transaction transaction);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/userCreateNew", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json)]//bare без обертки
        bool UserCreateNew(User user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/myBalanceUp", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json)]//bare без обертки
        bool MyBalanceUp(Transaction transaction);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/getMyTransactionHistoryWithAnotherUser", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]//bare без обертки
        List<Transaction> GetMyTransactionHistoryWithAnotherUser(Transaction transaction);


    }



}
