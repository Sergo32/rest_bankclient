﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TransferDataClassLibrary;
using WcfService.Data;
using TransferDataClassLibrary.Entities;

namespace WcfService
{

    public class ContractService : IContractService
    {
        private DbManager dbManager;

        public bool MyBalanceUp(Transaction transaction)
        {
            dbManager = DbManager.GetInstance();
            if (transaction != null)
            {

               // transaction.Dt = DateTime.Now;
                dbManager.TableTransaction.Insert(transaction);

                dbManager.TableUsers.IncreaseBalance(transaction.UserTo.Id, transaction.Money);
                return true;
            }


            return false;
        }

        public List<User> GetAllUser(string id)
        {
            dbManager = DbManager.GetInstance();
            List<User> users = dbManager.TableUsers.GetAllUsersWithoutMe(int.Parse(id));

            return users;
        }

        public int MyBalance(string id)
        {
            dbManager = DbManager.GetInstance();
            int balance = dbManager.TableUsers.GetMyBalance(int.Parse(id));
            return balance;
        }

        public bool UserCreateNew(User user)
        {

            dbManager = DbManager.GetInstance();
            if (dbManager.TableUsers.ChekLogin(user.Login))
            {
                return true;
            }
            else
            {
                dbManager.TableUsers.Insert(user);
                return false;
            }

        }

        public User UsersAuth(string login, string password)
        {
            bool exist = false;//запретить добовлять пользователя если такой логин сушествует 
            dbManager = DbManager.GetInstance();
            User findedUser = dbManager.TableUsers.SelectByLoginAndPassword(login, password);

            if (findedUser != null)
            {
                return findedUser;
            }

            return null;
        }

        public bool UserToUserTransaction(Transaction transaction)
        {
            dbManager = DbManager.GetInstance();
            if (transaction != null)
            {
                dbManager.TableTransaction.Insert(transaction);

                dbManager.TableUsers.IncreaseBalance(transaction.UserTo.Id, transaction.Money);

                dbManager.TableUsers.DecreaseBalance(transaction.UserFrom.Id, transaction.Money);
                return true;
            }
            return false;
        }

        public List<Transaction> GetMyTransactionHistoryWithAnotherUser(Transaction transaction)
        {
            dbManager = DbManager.GetInstance();
            List<Transaction> transactions =
                dbManager.TableTransaction.GetAllTransactionBetweenUsers(transaction.UserFrom.Id, transaction.UserTo.Id);

            return transactions;
        }
        

    }
}
